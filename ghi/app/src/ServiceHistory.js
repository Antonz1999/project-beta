import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'
import moment from 'moment'
function ServiceHistory () {
    const [appointments, setAppointments] = useState(null)
    const [filterTerm, setFilterTerm] = useState("")


    const getData = async () => {
        const resp = await fetch('http://localhost:8080/service/appointments/')
        if (resp.ok) {
            const data = await resp.json()
            setAppointments(data.appointments)

        }
    }

    const handleFilterChange = (e) => {
        setFilterTerm(e.target.value);
      };


    useEffect(() => {
        getData()

    },[])






    return ( <>
    {appointments && <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>VIN Appointment History</h1>
                <div className="input-group mb-3">
  <input onChange={handleFilterChange} type="text" className="form-control" placeholder="VIN" aria-label="VIN" aria-describedby="basic-addon2"/>
                </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Customer name</th>
                                <th>Date and Time</th>
                                <th>Reason</th>
                                <th>Technician</th>
                                <th>VIP?</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                appointments.filter((appointment) => appointment.automobile_vin.includes(filterTerm)).map(appointment => {
                                    const value = appointment.vip
                                    return (

                                        <>
                                        <tr key={appointment.id}>
                                            <td>{appointment.automobile_vin}</td>
                                            <td>{appointment.customer_name}</td>
                                            <td>{moment(appointment.date_time).format("MMMM Do YYYY, h:mm a")}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.technician.name}</td>
                                            <td>{String(value)}</td>
                                        </tr>
                                        </>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>}

    </>
    )
                        }


export default ServiceHistory;
