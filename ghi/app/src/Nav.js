import { NavLink, useNavigate } from 'react-router-dom';
import React, { useState } from 'react';

function Nav() {
  const navigate = useNavigate();
  const [selectedOption, setSelectedOption] = useState('');
  const handleOptionChange = (event) => {
    const path = event.target.value;
    setSelectedOption(path);
    navigate(path);
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <label>
              Manufacturers
              <select value={selectedOption} onChange={handleOptionChange}>
                <option value="">Choose an option</option>
                <option value="/manufacturers">Manufacturers</option>
                <option value="/manufacturers/new">Add a Manufacturer</option>
              </select>
            </label>
            <label>
              Vehicle Models
              <select value={selectedOption} onChange={handleOptionChange}>
                <option value="">Choose an option</option>
                <option value="/models">Vehicle Models List</option>
                <option value="/models/new">Add a New Vehicle Model</option>
              </select>
            </label>
            <label>
              Automobiles
              <select value={selectedOption} onChange={handleOptionChange}>
                <option value="">Choose an option</option>
                <option value="/automobiles">Automobiles</option>
                <option value="/automobiles/new">Add a New Automobile</option>
              </select>
            </label>
            <label>
              Appointments
              <select value={selectedOption} onChange={handleOptionChange}>
                <option value="">Choose an option</option>
                <option value="/services/appointments">Active Appointments</option>
                <option value="/services/appointments/new">Make an Appointment</option>
                <option value="/services/appointments/history">Service History</option>
                <option value="/technicians/new">Add a technician</option>
              </select>
            </label>
            <label>
              Sales
              <select value={selectedOption} onChange={handleOptionChange}>
                <option value="">Choose an option</option>
                <option value="/customer/new">Add a Customer</option>
                <option value="/salespeople/new">Add a Sales Person</option>
                <option value="/sales/list">List of Sales</option>
                <option value="/sales/new">Record a new sale</option>
                <option value="/sales/history">Sale person history</option>
              </select>
            </label>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
