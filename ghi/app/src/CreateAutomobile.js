import { useState, useEffect} from 'react'

function CreateAutomobile() {
const [models, setModels] = useState("")
const [formData, setForm] = useState({
    color: "",
    year: "",
    vin: "",
    model_id:"",
})
const fetchData = async () => {
const url = 'http://localhost:8100/api/models/'
const response = await fetch(url)
if (response.ok) {
    const data = await response.json()
    setModels(data.models)
}
    }
    useEffect(() => {
        fetchData()

    }, [])


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setForm({
                color: "",
                year: "",
                vin: "",
                model_id:"",
            })
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setForm({
            ...formData, [inputName]: value
        })
    }


    return (
        <>
        { models && <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Automobile</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="name">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="name">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="name">Vin</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} required value={formData.model_id} name="model_id" id="model_id" className="form-select">
                  <option value="">Choose a model</option>
                  {models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
}
</>
    )

}

export default CreateAutomobile
