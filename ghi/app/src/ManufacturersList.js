import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const ManufacturersList = () => {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/manufacturers/')
        if (resp.ok) {
            const data = await resp.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    // const handleDelete = async (e) => {
    //     const url = `http://localhost:8080/api/shoes/${e.target.id}`

    //     const fetchConfigs = {
    //         method: "Delete",
    //         headers: {
    //             "Content-Type": "application/json"
    //         }
    //     }

    //     const resp = await fetch(url, fetchConfigs)
    //     const data = await resp.json()

    //     setShoes(shoes.filter(shoe => String(shoe.id) !== e.target.id))
    // }

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Manufacturers</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                manufacturers.map(manufacturer => {
                                    return (
                                        <tr key={manufacturer.href}>
                                            <td>{manufacturer.name}</td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ManufacturersList;
