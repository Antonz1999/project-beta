import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

function AutomobileList () {
    const [autos, setAutos] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/')
        if (resp.ok) {
            const data = await resp.json()
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Automobiles</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Model Name</th>
                                <th>Color</th>
                                <th>Year</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                autos.map(auto => {
                                    return (
                                        <tr key={auto.href}>
                                            <td>{auto.model.name}</td>
                                            <td>{auto.color}</td>
                                            <td>{auto.year}</td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default AutomobileList;
