import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './CreateManufacturer';
import VehicleCreate from './VehicleCreate';
import ModelsList from './ModelsList';
import SaleForm from './SaleForm';
import CreateAutomobile from './CreateAutomobile';
import AutoMobileList from './AutomobileList';
import AppointmentList from './ListAppointments';
import CreateTechnician from './CreateTechnician';
import CreateAppointment from './CreateAppointment';
import SaleHistory from './SaleHistory';
import ServiceHistory from './ServiceHistory';
import CreateCustomer from './CreateCustomer';
import CreateSalesPerson from './CreateSalesPerson';
import ListSales from './ListSales';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="new" element={<VehicleCreate />} />
          </Route>
          <Route path="customer/new" element={<CreateCustomer/>}/>
          <Route path="salespeople/new" element={<CreateSalesPerson/>}/>
          <Route path="sales">
            <Route path="new" element={<SaleForm />} />
            <Route path="list" element={<ListSales />} />
            <Route path="history" element={<SaleHistory />} />
          </Route>
          <Route path="automobiles">
          <Route index element={<AutoMobileList />} />
            <Route path="new" element={<CreateAutomobile />} />
          </Route>
          <Route path="services">
          <Route path="appointments">
            <Route index element={<AppointmentList />}/>
            <Route path="new" element={<CreateAppointment />}/>
            <Route path="history" element={<ServiceHistory />}/>
            </Route>
          </Route>
          <Route path="technicians">
            <Route path="new" element={<CreateTechnician />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
