import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const ModelsList = () => {
    const [models, setModels] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/models/')
        if (resp.ok) {
            const data = await resp.json()
            setModels(data.models)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Vehicle Models</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                models.map(model => {
                                    return (
                                        <tr key={model.href}>
                                            <td>{model.name}</td>
                                            <td>{model.manufacturer.name}</td>
                                            <td><img src={model.picture_url} height="170" width="320"></img></td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ModelsList;
