from django.urls import path
from .views import api_salespeople, api_customers, api_sales_records, api_check_auto_vo

urlpatterns = [
    path("salespeople/", api_salespeople, name="api_salespeople"),
    path("customers/", api_customers, name="api_customers"),
    path("salesrecords/", api_sales_records, name="api_sales_records"),
    path("autovo/", api_check_auto_vo, name="api_check_auto_vo"),
]
