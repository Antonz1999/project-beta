from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment, AutomobileVO


@require_http_methods(["GET","POST"])
def list_technician(request):
    if request.method == "POST":
        content = request.body
        data = json.loads(content)
        technician = Technician.objects.create(**data)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
    else:
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments}, encoder=AppointmentEncoder, safe=False)
    else:
        content = request.body
        data = json.loads(content)
        try:
            AutomobileVO.objects.get(vin=data["automobile_vin"])
            data["vip"] = True
        except AutomobileVO.DoesNotExist:
            data["vip"] = False
        try:
            technician = Technician.objects.get(employee_number=data["technician"])
            technician.reserved = "yes"
            technician.save()
            data["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse({"message": "ivalid employee id"})
        data["finished"] = "no"
        appointment = Appointment.objects.create(**data)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["PUT", "DELETE"])
def appointment_details(request,id):
    if request.method == "DELETE":
        appointment = Appointment.objects.get(id=id)
        technician = Technician.objects.get(employee_number = appointment.technician.employee_number)
        technician.reserved = "no"
        technician.save()
        count,_ = appointment.delete()
        return JsonResponse({"deleted": count > 1})
    else:
        data = json.loads(request.body)
        try:
            AutomobileVO.objects.get(vin=data[0]["automobile_vin"])
            data[0]["vip"] = True
        except AutomobileVO.DoesNotExist:
            data[0]["vip"] = False
        try:
            technician = Technician.objects.get(employee_number=data[0]["technician"]["employee_number"])
            technician.reserved = "no"
            technician.save()
            data[0]["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "ivalid employee id"})
        Appointment.objects.filter(id=id).update(**data[0])
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)




# Create your views here.
