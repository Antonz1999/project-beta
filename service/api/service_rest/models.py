from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=250)


class Technician(models.Model):
    name = models.CharField(max_length=250)
    employee_number = models.CharField(max_length=250, unique=True)
    reserved = models.CharField(max_length=3)


class Appointment(models.Model):
    customer_name = models.CharField(max_length=250)
    reason = models.CharField(max_length=250)
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    automobile_vin = models.CharField(max_length=250, null=True)
    technician = models.ForeignKey(Technician, related_name="appointment", on_delete=models.CASCADE)
    finished = models.CharField(max_length=3)
    vip = models.BooleanField(default=False)
