from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class TechnicianEncoder(ModelEncoder):
    model =  Technician
    properties = ["name", "employee_number", "reserved"]


class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ["customer_name", "reason", "date_time", "automobile_vin", "technician","id", "vip", "finished"]
    encoders = {
        "technician": TechnicianEncoder()
    }
